1. 
Для небольшого объема данных
```sql
SELECT id, title, text
FROM article
WHERE id NOT IN (SELECT article_id FROM comment);
```
Для большого объема данных
```sql
SELECT article.id, article.title, article.text
FROM article
LEFT JOIN comment ON article.id = comment.article_id
WHERE comment.article_id IS NULL;
```
2.
Я написал код с вводом данных с клавиатуры, но если нужно именно с файла, могу изменить
```sh
stats = {}

while True:
    person_stat = input('Имя человека и количество часов:').split()
    if not person_stat:
        break
    
    hours = person_stat.pop()
    name = ' '.join(person_stat)

    if name in stats:
        stats[name].append(int(hours))
    else:
        stats[name] = [int(hours)]

for name, hours in stats.items():
    total_time = sum(hours)
    hours = ', '.join(map(str, hours))
    print(f'{name}: {hours}; sum: {total_time}')
```
